/*
Booking System API - Business Use Case Translation to Model Design

	Objectives:
	- Define models suitable for a booking system API
	- Setup booking system API and its dependencies
	
	Topics
	- Booking System MVP (Minimum Viable Product) requirements
	- Booking System API dependencies
	- Model Definition for Users and Courses
	

Booking System MVP Requirements
	- Minimum Viable Product (MVP) is a product that has enough features to be useful to its target market. It is used to validate a product idea at the onset of development.

	Our booking system MVP have the following features:
	- User registration
	- User authentication (login)
	- Retrieval of authenticated user's details
	- Course creation by an authenticated user
	- Retrieval of courses
	- Course info updating by authenticated user
	- Course archiving by an authenticated user

	Besides express and mongoose, we have the following additional packages:

	Booking System API Dependencies
	- bcrypt -- for hashing user password prior to storage. Encrypting passwords into random characters. A library to help you hash passwords.
	- cors -- for allowing cross-origin resource sharing. Allows certain features of our backend to be used by other resources. CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.
	- jsonwebtoken -- for implementing JSON web tokens. 
*/

const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
const cors = require("cors");
// Allows access to routes defined within the application
const userRoutes = require("./routes/user");
const courseRoutes = require("./routes/course");

const app = express();

// Connect to our MongoDB database
mongoose.connect("mongodb+srv://admin:12345@cluster0.p2t9q.mongodb.net/S32-S36?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Prompts a message for successful database connection
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'))

// Allows all resources to access the backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);
// Defines the "course" string to be included for all course routes in the "course" route file
app.use("/courses", courseRoutes);

// App listening to port 4000
// process.env.PORT - to deploy the server (environment variable) "CLOUD/heroku"
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
});