const jwt = require("jsonwebtoken");

// User defined string data that will be used to create JSON web tokens
const secret = "CourseBookingAPI";

// Token creation
module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

// Token verification - rechecks/secures token
module.exports.verify = (req, res, next) => {
	// the token is retrieved from the request header
	let token = req.headers.authorization

	// Token received is not undefined
	if(typeof token !== "undefined"){
		console.log(token);

		token = token.slice(7, token.length);

		// Validate the token using JWT's "verify" method, and decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			// If JWT is not valid
			if(err){
				return res.send({auth : "failed"});
			}
			// if JWT is valid
			else{
				// Allows the application with the next middleware function/callback function
				next();
			}
		})
	}
	// Token does not exist
	else{
		return res.send({auth : "failed"});
	}
}

// Token decyption
module.exports.decode = (token) => {
	//Token received is not undefined
	if(typeof token !== "undefined"){
		// Retrieves only the token and removes the "Bearer" prefix
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if(err){
				return null;
			}
			else{
				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional inforamation from the JWT
				// The "payload" property contains information provided in the "createAccesstoken" method
				return jwt.decode(token, {complete:true}).payload
			}
		})
	}
	//Token does not exist
	else{
		return null;
	}
}