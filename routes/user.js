/*
	Topics:
	1. User Registration
		- Implement the necessary route and controller action necessary for registering a user
	2. User Authentication
		- Implement the necessary route and controller action for authentication a user
	3. Json Web Token
		- An object that securely transmits information between parties in a compact and self-contained way. Key to fully access the full feature of the application.
		- Integrate the use of a JsonWebToken in the user authentication wokflow
*/


const express = require("express");
const router = express.Router();
const userController = require("../controllers/user")
const auth = require("../auth")


// Route checking if the user's email already exists in the database
// endpoint: localhost:4000/users/checkEmail
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


// Act
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	// Provides the user's ID for the getUser controller method
	userController.getUser({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// Route to enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {
	let data = auth.decode({ // Located on models
		userId : req.body.userId,
		courseId : req.body.courseId
	})

	userController.enroll(data).then(resultFromController => res.send(resultFromController))


})

module.exports = router;